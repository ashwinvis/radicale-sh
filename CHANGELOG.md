
# 0.1.0 — 2021-10-06

### Added

- Tool `scriv` as development dependency

### Changed

- Rename package from `radicale-sh` to `radicale-auth-sh`
- Replaced command line script `radicale-sh-init` with module based invocation `python -m radicale_auth_sh.config`

# 0.0.1 — 2021-03-04

### Added

- First version
